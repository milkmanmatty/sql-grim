﻿using SQLGrim.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace SQLGrim {
	/// <summary>
	/// Interaction logic for MainPage.xaml
	/// </summary>
	public partial class MainPage : Page {

		public ObservableCollection<VisTable> Tables { get; set; }

		private readonly BackgroundWorker sqlWorker = new BackgroundWorker();
		public static DataTable dt { get; set; }
		public static SqlCommand Query { get; set; }
		public static string QueryString { get; set; }
		public static Message Msg = new Message();
		private bool ScriptsOpen = false;
		private bool ScriptsAdd = false;
		private bool ScriptNameIsValid = true;

		public MainPage() {
			InitializeComponent();
			sqlWorker.DoWork += Worker_DoWork;
			sqlWorker.RunWorkerCompleted  += Worker_RunWorkerCompleted;
			sqlWorker.WorkerSupportsCancellation = true;
			dt = new DataTable();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			using (SqlConnection conn = new SqlConnection()) {
				conn.ConnectionString = UserSettings.ConString;
				conn.Open();
				var dts = conn.GetSchema("Tables").AsEnumerable();
				Tables = new ObservableCollection<VisTable>();
				foreach(var dt in dts){
					Tables.Add(new VisTable(dt));
				}
			}
			System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(UserSettings.ConString);
			Database.Text = "Connected To: "+builder.DataSource+"/"+builder.InitialCatalog;
			MessageBlock.Text = "Ready.";
			listView1.ItemsSource = Tables;
			ScriptsList.ItemsSource = UserSettings.Scripts;
		}

		#region Button Clicks/Events

		private void ListView1_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
			if(sender == null || ((ListView)sender).SelectedItem == null){
				return;
			}
			GetSelectedQuery().Document.Blocks.Clear();
			UserSettings.DebugBP();

			string[] parts = UserSettings.DefaultQuery.Split(new[] {"{TABLE}"}, StringSplitOptions.None);

			if(parts.Length < 1){ 
				GetSelectedQuery().Document.Blocks.Add(new Paragraph( new Run( UserSettings.DEF_DefaultQuery + ((ListView)sender).SelectedItem.ToString() )));
			} else if(parts.Length > 1) {
				GetSelectedQuery().Document.Blocks.Add(new Paragraph( new Run( parts[0] + ((ListView)sender).SelectedItem.ToString() + parts[1] )));
			} else {
				GetSelectedQuery().Document.Blocks.Add(new Paragraph( new Run( parts[0] + ((ListView)sender).SelectedItem.ToString() )));
			}
			TextFormat.Run(this, GetSelectedQuery());
		}

		private void Settings_Click(object sender, RoutedEventArgs e) {
			SettingsPage setpage = new SettingsPage();
			this.NavigationService.Navigate(setpage);
		}
		
		private void Scripts_Click(object sender, RoutedEventArgs e) {
			ScriptsOpen = !ScriptsOpen;
			if(ScriptsOpen){
				var anim = new DoubleAnimation(UserSettings.ScriptsPanelWidth, (Duration)TimeSpan.FromSeconds(0.3));
				ScriptsPanel.BeginAnimation(ContentControl.WidthProperty, anim);
			} else {
				var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(0.3));
				anim.Completed += (s, _) => { ScriptsCol.Width = new GridLength(0,GridUnitType.Auto); };
				ScriptsPanel.BeginAnimation(ContentControl.WidthProperty, anim);
			}
		}
		
		private void ExecuteQuery_Click(object sender, RoutedEventArgs e) {
			ExecuteQuery();
		}
		private void ExecuteQuery(){
			StopExBtn.IsEnabled = true;
			Msg.Text = "Executing SQL..."; Msg.Colour = "ff9";
			SetMsg();
			CancelQuery();
			QueryString = GetText();
			sqlWorker.CancelAsync();
			sqlWorker.RunWorkerAsync();
		}
		
		private void StopExecution_Click(object sender, RoutedEventArgs e) {
			CancelQuery();
			sqlWorker.CancelAsync();
		}

		private void Query_KeyUp(object sender, KeyEventArgs e) {
			if(e.Key == Key.F5){
				ExecuteQuery();
				return;
			}
			TextFormat.Run(this, GetSelectedQuery());
		}

		/* Handle illegal characters in Script Name. 
		 * Some characters are not allowed since the the Script Name is saved as the name for a file, and operating systems do not like some charactes in the name */
		private void NewScriptName_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			if (!TextFormat.IsAlphaNumeric( e.Text )) {
				Msg.Text = "Illegal character removed from Script Name.";
				Msg.Colour = "ff9";
				SetMsg();
				ScriptNameIsValid = false;
				e.Handled = true;
				return;
			}
		}
		private void NewScriptName_TextChanged(object sender, TextChangedEventArgs e) {
			if (!TextFormat.IsAlphaNumeric(NewScriptName.Text)) {
				Msg.Text = "Illegal characters present in Script Name.";
				Msg.Colour = "f99";
				SetMsg();
				ScriptNameIsValid = false;
				return;
			}
			if(!ScriptNameIsValid){
				ScriptNameIsValid = true;
				Msg.Text = "Ready.";
				Msg.Colour = "ff9";
				SetMsg();
			}
		}

		private void ScriptNewQuery_KeyUp(object sender, KeyEventArgs e) {
			TextFormat.Run(this, ScriptNewQuery);
		}

		private void CreateNewScript_Click(object sender, RoutedEventArgs e) {
			ToggleNewScript();
			//Reset Fields
			NewScriptName.Text = "";
			NewScriptTags.Text = "";
			ScriptNewQuery.Document.Blocks.Clear();
			var p = new Paragraph();
			p.Inlines.Add("--Let the fun begin!");
			ScriptNewQuery.Document.Blocks.Add(p);
		}

		private void EditScript_Click(object sender, RoutedEventArgs e) {
			string name = ((Button)sender).Tag.ToString();
			ToggleEditScript(name);
			TextFormat.Run(this, ScriptNewQuery);
		}

		private void BackScript_Click(object sender, RoutedEventArgs e) {
			ToggleNewScript();
		}

		private void SaveNewScript_Click(object sender, RoutedEventArgs e) {
			string newName = NewScriptName.Text.Trim();
			if(!String.IsNullOrEmpty(newName) && TextFormat.IsAlphaNumeric(newName)){
				try {
					Script s = new Script(){
						Name = newName,
						Query = new TextRange(ScriptNewQuery.Document.ContentStart, ScriptNewQuery.Document.ContentEnd).Text.Trim(),
						Tags = NewScriptTags.Text.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries)
					};
					UserSettings.Scripts.Add(s);
					UserSettings.SaveScripts();
					Msg.Colour = "9f9";
					Msg.Text = "New Script saved!";
				} catch {
					Msg.Colour = "f99";
					Msg.Text = "Could not save Script.";
				}
			} else {
				Msg.Colour = "f99";
				Msg.Text = "Could not save Script, invalid Name.";
			}
			SetMsg();
		}

		private void UpdateScript_Click(object sender, RoutedEventArgs e) {
			string oldName = EditScriptName.Text.Trim();
			string newName = NewScriptName.Text.Trim();
			var s = UserSettings.Scripts.Where(x => x.Name == oldName).FirstOrDefault();

			if(s == null){
				Msg.Colour = "f99";
				Msg.Text = "Could not update Script, invalid Name.";
				SetMsg();
				return;
			}

			if(!String.IsNullOrEmpty(newName) && TextFormat.IsAlphaNumeric(newName)){
				try {
					//Add new script
					Script scr = new Script(){
						Name = newName,
						Query = new TextRange(ScriptNewQuery.Document.ContentStart, ScriptNewQuery.Document.ContentEnd).Text.Trim(),
						Tags = NewScriptTags.Text.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries)
					};
					UserSettings.Scripts.Add(scr);

					//remove old script
					UserSettings.Scripts.Remove(s);

					//Save
					UserSettings.SaveScripts();

					Msg.Colour = "9f9";
					Msg.Text = "Script updated!";
				} catch {
					Msg.Colour = "f99";
					Msg.Text = "Could not update Script.";
				}
			} else {
				Msg.Colour = "f99";
				Msg.Text = "Could not save Script, invalid Name.";
			}
			SetMsg();
		}

		protected void ScriptList_HandleDoubleClick(object sender, MouseButtonEventArgs e) {

			if (((FrameworkElement)e.OriginalSource).DataContext is Script script) {
				RichTextBox newRTB = new RichTextBox();
				newRTB.Document.Blocks.Clear();
				Paragraph p = new Paragraph();
				p.Inlines.Add(script.Query);
				newRTB.Document.Blocks.Add(p);
				newRTB.KeyUp += Query_KeyUp;

				BrushConverter bc = new BrushConverter();
				Brush brush1=(Brush)bc.ConvertFrom("#212121");
				brush1.Freeze();
				newRTB.Background = brush1;
				Brush brush2=(Brush)bc.ConvertFrom("#eee");
				brush2.Freeze();
				newRTB.Foreground = brush2;

				TextFormat.Run(this,newRTB);

				TabItem newTI = new TabItem()
				{
					Header = script.Name,
					Content = newRTB
				};
				TabQueries.Items.Add(newTI);
				Dispatcher.BeginInvoke((Action)(() => TabQueries.SelectedIndex = TabQueries.Items.Count-1));
				Msg.SetMsg("Script Loaded.", "ff9");
				SetMsg();
			}
		}

		/// <summary>
		/// Overridden in TextFormat.Run().
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		internal void TbQuery_TextChanged(object sender, TextChangedEventArgs e) {}

		#endregion

		#region Worker

		private void Worker_DoWork(object sender, DoWorkEventArgs e) {
			RunSQL(QueryString);
		}

		private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
			//update ui once worker complete his work
			if(dt != null){
				ResultsGrid.ItemsSource = null;
				ResultsGrid.ItemsSource = dt.DefaultView;
			}
			SetMsg();
			StopExBtn.IsEnabled = false;
		}

		#endregion

		/// <summary>
		/// Check for the presence of UPDATE or DELETE sql keywords and if found reports whether the existence of the WHERE keyword is found.
		/// Does not check if each UPDATE/DELETE keyword has an associated WHERE keyword, only if a WHERE keyword exists in hte string.
		/// </summary>
		/// <param name="sql"></param>
		/// <returns>false if string contains "update" or "delete" and does not contain "where", true otherwise.</returns>
		private bool ValidateSQL(string sql){
			sql = sql.ToUpper();
			if((sql.Contains("UPDATE") || sql.Contains("DELETE")) && !sql.Contains("WHERE")){
				return false;
			}
			return true;
		}

		private void CancelQuery(){
			try {
				Query.Cancel();
			} catch (NullReferenceException e){
				Msg.SetMsg("Query stopped by user.","ffff99");
				SetMsg();
			}
		}

		private void RunSQL(string query){
			if(ValidateSQL(query)){
				try {
					using (SqlConnection conn = new SqlConnection()) {
						conn.ConnectionString = UserSettings.ConString;
						conn.Open();

						Query = new SqlCommand(query, conn);

						using(SqlDataAdapter adapter = new SqlDataAdapter(Query)){
							dt = new DataTable();
							adapter.Fill(dt);
						}
					}
					Msg.SetMsg("SQL executed.","99ff99");
				} catch (Exception e) {
					Msg.SetMsg(e.Message, "ff9999");
				}
			} else {
				Msg.SetMsg("No WHERE clause specified. SQL did not execute.","ff9999");
			}
			return;
		}

		private void ToggleNewScript(){
			ScriptsAdd = !ScriptsAdd;
			BooleanToVisibilityConverter vc = new BooleanToVisibilityConverter();
			
			//Little hard to read but ommits a branching operation.
			CreateScriptToolbar.Visibility = (Visibility)vc.Convert(!ScriptsAdd,null,null,null);
			SaveScriptToolbar.Visibility = (Visibility)vc.Convert(ScriptsAdd,null,null,null);
			ScriptsList.Visibility = (Visibility)vc.Convert(!ScriptsAdd,null,null,null);
			ScriptNewEdit.Visibility = (Visibility)vc.Convert(ScriptsAdd,null,null,null);
			UpdateScriptToolbar.Visibility = (Visibility)vc.Convert(false,null,null,null);
		}

		private void ToggleEditScript(string name){
			ScriptsAdd = !ScriptsAdd;
			BooleanToVisibilityConverter vc = new BooleanToVisibilityConverter();
			
			//Little hard to read but ommits a branching operation.
			CreateScriptToolbar.Visibility = (Visibility)vc.Convert(!ScriptsAdd,null,null,null);
			SaveScriptToolbar.Visibility = (Visibility)vc.Convert(false,null,null,null);
			UpdateScriptToolbar.Visibility = (Visibility)vc.Convert(ScriptsAdd,null,null,null);
			ScriptsList.Visibility = (Visibility)vc.Convert(!ScriptsAdd,null,null,null);
			ScriptNewEdit.Visibility = (Visibility)vc.Convert(ScriptsAdd,null,null,null);

			//Find script that has been edited
			var s = UserSettings.Scripts.Where(x => x.Name == name).FirstOrDefault();

			//Set Name
			NewScriptName.Text = s.Name;
			EditScriptName.Text = s.Name;

			//Set Query
			ScriptNewQuery.Document.Blocks.Clear();
			var p = new Paragraph();
			p.Inlines.Add(s.Query);
			ScriptNewQuery.Document.Blocks.Add(p);

			//Set Tags
			NewScriptTags.Text = string.Join(",",s.Tags);
		}

		private void SetMsg(){
			BrushConverter bc = new BrushConverter();
			Brush brush=(Brush)bc.ConvertFrom("#"+Msg.Colour);
			brush.Freeze();
			MessageBlock.Foreground = brush;
			MessageBlock.Text = Msg.Text;
		}

		private string GetText(){
			return new TextRange(GetSelectedQuery().Document.ContentStart, GetSelectedQuery().Document.ContentEnd).Text;
		}

		private RichTextBox GetSelectedQuery(){
			return (RichTextBox)TabQueries.SelectedContent;
		}
	}

	public class Message {
		public string Text { get; set; }
		public string Colour { get; set; }

		public void SetMsg(string msg, string hexColour){
			Colour = hexColour;
			Text = msg;
		}
	}
}
