﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SQLGrim {
	/// <summary>
	/// Interaction logic for ConnectionPage.xaml
	/// </summary>
	public partial class SettingsPage : Page {
		public SettingsPage() {
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			Set_ConString.Text = UserSettings.ConString;
			Set_SaveConFile.IsChecked = UserSettings.SaveConStringToFile;
			Set_DefQuery.Text = UserSettings.DefaultQuery;
			Set_ReservedWordsColour.Text = UserSettings.ReservedWordsColour;
			Set_ReservedWordsBold.IsChecked = UserSettings.ReservedWordsBolded;
			Set_SPCCharsColour.Text = UserSettings.SpecialCharsColour;
			Set_SPCCharsBold.IsChecked = UserSettings.SpecialCharsBolded;
			Set_ScriptsPanelWidth.Text = UserSettings.ScriptsPanelWidth.ToString();
		}

		#region Event Handlers

		/// <summary>
		/// Navigate back to the main page.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Back_Clicked(object sender, RoutedEventArgs e) {
			this.NavigationService.Navigate(new MainPage());
		}

		/// <summary>
		/// Tests the user-entered connection string. If the connection string can be opened successfully then it is saved as the new connection string.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ConnectBtn_Click(object sender, RoutedEventArgs e) {

			string testConStr = Set_ConString.Text.Trim();
			SetMsg(new Message(){ Text="Testing Connection...", Colour="ffff99" });

			using (SqlConnection conn = new SqlConnection()) {
				conn.ConnectionString = testConStr;
				try {
					conn.Open();
					SetMsg(new Message(){ Text="Connected to new Database!", Colour="99ff99" });
					UserSettings.ConString = testConStr;
					MainPage mainpage = new MainPage();
					this.NavigationService.Navigate(mainpage);
				}
				catch (SqlException) {
					SetMsg(new Message(){ Text="Could not connect.", Colour="ff9999" });
				}
			}
		}

		/// <summary>
		/// Save UserSettings.DefaultQuery on loss of focus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Set_DefQuery_LostFocus(object sender, RoutedEventArgs e) {
			UserSettings.DefaultQuery = Set_DefQuery.Text;
			SetMsg(new Message(){ Text="'Default Query' setting saved!", Colour="99ff99" });
			UserSettings.SaveToFile();
		}

		/// <summary>
		/// Save UserSettings.ReservedWordsColour on loss of focus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Set_ReservedWordsColour_LostFocus(object sender, RoutedEventArgs e) {
			if(!TextFormat.IsHex(Set_ReservedWordsColour.Text.Replace("#",""))){
				SetMsg(new Message(){ Text="Invalid (Non-Hexidecimal) value entered for 'Reserved Words Colour'.", Colour="ff9999" });
				return;
			}
			UserSettings.ReservedWordsColour = Set_ReservedWordsColour.Text;
			SetMsg(new Message(){ Text="'Reserved Words Colour' setting saved!", Colour="99ff99" });
			UserSettings.SaveToFile();
		}

		/// <summary>
		/// Save UserSettings.SpecialCharsColour on loss of focus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Set_SPCCharsColour_LostFocus(object sender, RoutedEventArgs e) {
			if(!TextFormat.IsHex(Set_SPCCharsColour.Text.Replace("#",""))){
				SetMsg(new Message(){ Text="Invalid (Non-Hexidecimal) value entered for 'SPC Chars Colour'.", Colour="ff9999" });
				return;
			}
			UserSettings.SpecialCharsColour = Set_SPCCharsColour.Text;
			SetMsg(new Message(){ Text="'SPC Chars Colour' setting saved!", Colour="99ff99" });
			UserSettings.SaveToFile();
		}

		/// <summary>
		/// Save UserSettings.ScriptsPanelWidth on loss of focus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Set_ScriptsPanelWidth_LostFocus(object sender, RoutedEventArgs e) {
			if(!TextFormat.IsNumber(Set_SPCCharsColour.Text)){
				SetMsg(new Message(){ Text="Invlid (Non-Numeric) value entered for 'SPC Chars Colour'.", Colour="ff9999" });
				return;
			}
			int old = UserSettings.ScriptsPanelWidth;
			try{ 
				UserSettings.ScriptsPanelWidth = Int32.Parse(Set_ScriptsPanelWidth.Text);
				SetMsg(new Message(){ Text="'Scripts Panel Width' setting saved!", Colour="99ff99" });
				UserSettings.SaveToFile();
			} catch {
				UserSettings.ScriptsPanelWidth = old;
				SetMsg(new Message(){ Text="'Scripts Panel Width' setting FAILED to save!", Colour="ff9999" });
			}
		}

		/// <summary>
		/// On Dock focus (click) fire all the "LostFocus" events.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Dock_Focus(object sender, MouseButtonEventArgs e) {
			if(Set_DefQuery.IsFocused || Set_DefQuery.IsKeyboardFocused){ 
				Set_DefQuery_LostFocus(null,null);
				Set_ReservedWordsColour_LostFocus(null,null);
				Set_SPCCharsColour_LostFocus(null,null);
				Set_ScriptsPanelWidth_LostFocus(null,null);
			}
			Keyboard.ClearFocus();
			SettingsMsg.Focus();
		}

		/// <summary>
		/// Save UserSettings.SaveConStringToFile on checkbox click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Set_SaveConFile_Click(object sender, RoutedEventArgs e) {
			UserSettings.SaveConStringToFile = ((CheckBox)sender).IsChecked.Value;
			SetMsg(new Message(){ Text="'Save Connection String to file' setting saved!", Colour="99ff99" });
			UserSettings.SaveToFile();
		}
		#endregion

		/// <summary>
		/// Sets the message box's text and colour so that the user can visually see feedback.
		/// </summary>
		/// <param name="Msg">The Message containing the desired text and colour to be displayed to the user.</param>
		private void SetMsg(Message Msg){
			BrushConverter bc = new BrushConverter();
			Brush brush=(Brush)bc.ConvertFrom("#"+Msg.Colour);
			brush.Freeze();
			SettingsMsg.Foreground = brush;
			SettingsMsg.Text = Msg.Text;
		}
	}
}
