﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace SQLGrim {
	public static class TextFormat {

		public static string[] reservedWords = {
			"ADD", "EXTERNAL", "PROCEDURE", "ALL", "FETCH", "PUBLIC", "ALTER", "FILE", "RAISERROR", "AND", "FILLFACTOR", "READ", "ANY", "FOR", "WRITETEXT",
			"READTEXT", "AS", "FOREIGN", "RECONFIGURE", "ASC", "FREETEXT", "EXIT", "REFERENCES", "AUTHORIZATION", "FREETEXTTABLE", "REPLICATION", "BACKUP",
			"FROM", "RESTORE", "BEGIN", "FULL", "RESTRICT", "BETWEEN", "FUNCTION", "RETURN", "BREAK", "GOTO", "REVERT", "BROWSE", "GRANT", "REVOKE", "BULK",
			"GROUP", "RIGHT", "BY", "HAVING", "ROLLBACK", "CASCADE", "HOLDLOCK", "ROWCOUNT", "CASE", "IDENTITY", "ROWGUIDCOL", "CHECK", "IDENTITY_INSERT",
			"RULE", "CHECKPOINT", "IDENTITYCOL", "SAVE", "CLOSE", "IF", "SCHEMA", "CLUSTERED", "IN", "SECURITYAUDIT", "COALESCE", "INDEX", "SELECT", "OPTION",
			"COLLATE", "INNER", "SEMANTICKEYPHRASETABLE", "COLUMN", "INSERT", "PROC", "SEMANTICSIMILARITYDETAILSTABLE", "COMMIT", "INTERSECT", "SEMANTICSIMILARITYTABLE",
			"COMPUTE", "INTO", "SESSION_USER", "CONSTRAINT", "IS", "SET", "CONTAINS", "JOIN", "SETUSER", "CONTAINSTABLE", "KEY", "SHUTDOWN", "CONTINUE", "KILL",
			"SOME", "CONVERT", "LEFT", "STATISTICS", "CREATE", "LIKE", "SYSTEM_USER", "CROSS", "LINENO", "TABLE", "CURRENT", "LOAD", "TABLESAMPLE", "CURRENT_DATE",
			"MERGE", "TEXTSIZE", "CURRENT_TIME", "NATIONAL", "THEN", "CURRENT_TIMESTAMP", "NOCHECK", "TO", "CURRENT_USER", "NONCLUSTERED", "TOP", "CURSOR", "NOT",
			"TRAN", "DATABASE", "NULL", "TRANSACTION", "DBCC", "NULLIF", "TRIGGER", "DEALLOCATE", "OF", "TRUNCATE", "DECLARE", "OFF", "TRY_CONVERT", "DEFAULT",
			"OFFSETS", "TSEQUAL", "DELETE", "ON", "UNION", "DENY", "OPEN", "UNIQUE", "DESC", "OPENDATASOURCE", "UNPIVOT", "DISK", "OPENQUERY", "UPDATE", "DISTINCT",
			"OPENROWSET", "UPDATETEXT", "DISTRIBUTED", "OPENXML", "USE", "DOUBLE", "USER", "DROP", "OR", "VALUES", "DUMP", "ORDER", "VARYING", "ELSE", "OUTER",
			"VIEW", "END", "OVER", "WAITFOR", "ERRLVL", "PERCENT", "WHEN", "ESCAPE", "PIVOT", "WHERE", "EXCEPT", "PLAN", "WHILE", "EXEC", "PRECISION", "WITH",
			"EXECUTE", "PRIMARY", "WITHIN", "GROUP", "EXISTS", "PRINT"
		};
		public static char[] spcChars = { '.', ')', '(', '[', ']', '>', '<', ':', ';', '*' };

		struct Tag {
			public TextPointer StartPosition;
			public TextPointer EndPosition;
			public string Word;
		}
		private static List<Tag> tags = new List<Tag>();
		private static List<Tag> chars = new List<Tag>();
		private static List<Tag> cmnts = new List<Tag>();

		#region Syntax Highlighting Methods
		public static void Run(Object _class, RichTextBox rtb){

			FlowDocument doc  = rtb.Document;
			TextRange documentRange = new TextRange(doc.ContentStart, doc.ContentEnd);
			documentRange.ClearAllProperties();

			TextPointer navigator = doc.ContentStart;

			while (navigator.CompareTo(doc.ContentEnd) < 0) {
				TextPointerContext context = navigator.GetPointerContext(LogicalDirection.Backward);
				if (context == TextPointerContext.ElementStart && navigator.Parent is Run) {
					CheckWordsInRun((Run)navigator.Parent);
				}
				navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
			}

			rtb.TextChanged -= ((MainPage)_class).TbQuery_TextChanged;

			for (int i = 0; i < tags.Count; i++) {
				TextRange range = new TextRange(tags[i].StartPosition, tags[i].EndPosition);
				range.ApplyPropertyValue(TextElement.ForegroundProperty, (SolidColorBrush)(new BrushConverter().ConvertFromString(UserSettings.ReservedWordsColour)));
				if(UserSettings.ReservedWordsBolded){ range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold); }
			}
			tags.Clear();
			for (int i = 0; i < chars.Count; i++) {
				TextRange range = new TextRange(chars[i].StartPosition, chars[i].EndPosition);
				range.ApplyPropertyValue(TextElement.ForegroundProperty, (SolidColorBrush)(new BrushConverter().ConvertFromString(UserSettings.SpecialCharsColour)));
				if(UserSettings.SpecialCharsBolded){ range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold); }
			}
			chars.Clear();
			for (int i = 0; i < cmnts.Count; i++) {
				TextRange range = new TextRange(cmnts[i].StartPosition, cmnts[i].EndPosition);
				range.ApplyPropertyValue(TextElement.ForegroundProperty, (SolidColorBrush)(new BrushConverter().ConvertFromString(UserSettings.CommentsColour)));
				if(UserSettings.SpecialCharsBolded){ range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold); }
			}
			cmnts.Clear();

			rtb.TextChanged += new TextChangedEventHandler(((MainPage)_class).TbQuery_TextChanged);
		}

		private static void CheckWordsInRun(Run run){
			int sIndex = 0;
			int eIndex = 0;
			string text = run.Text;

			bool commentStarted = false;

			for (int i = 0; i < text.Length; i++) {

				if(commentStarted && (text[i] == '\r' || text[i] == '\n')){
					eIndex = i-1;
					string word = text.Substring(sIndex, eIndex - sIndex + 1);
					Tag t = new Tag(){
						StartPosition = run.ContentStart.GetPositionAtOffset(sIndex, LogicalDirection.Forward),
						EndPosition = run.ContentStart.GetPositionAtOffset(eIndex + 1, LogicalDirection.Backward),
						Word = word
					};
					cmnts.Add(t);
					commentStarted = false;

				} else if(text[i] == '-' && (i+1 < text.Length && text[i+1] == '-')){
					commentStarted = true;
					sIndex = i;
				}
				if(!commentStarted){
					if (Char.IsWhiteSpace(text[i]) || spcChars.Contains(text[i])) {
						if(spcChars.Contains(text[i])) {
							char c = text[i];
							if (spcChars.Contains(c)) {
								Tag t = new Tag() {
									StartPosition = run.ContentStart.GetPositionAtOffset(i, LogicalDirection.Forward),
									EndPosition = run.ContentStart.GetPositionAtOffset(i+1, LogicalDirection.Backward),
									Word = c.ToString()
								};
								chars.Add(t);
							}
						} else if (i > 0){
							if(!(Char.IsWhiteSpace(text[i - 1]) || spcChars.Contains(text[i - 1]))) {
								eIndex = i - 1;
								string word = text.Substring(sIndex, eIndex - sIndex + 1);
								if (reservedWords.Contains(word, StringComparer.OrdinalIgnoreCase)) {
									Tag t = new Tag() {
										StartPosition = run.ContentStart.GetPositionAtOffset(sIndex, LogicalDirection.Forward),
										EndPosition = run.ContentStart.GetPositionAtOffset(eIndex + 1, LogicalDirection.Backward),
										Word = word
									};
									tags.Add(t);
								}
							}
						}
						sIndex = i + 1;
					}
				}
			}
			
			string lastWord = text.Substring(sIndex, text.Length - sIndex);
			if (reservedWords.Contains(lastWord, StringComparer.OrdinalIgnoreCase)) {
				Tag t = new Tag();
				t.StartPosition = run.ContentStart.GetPositionAtOffset(sIndex, LogicalDirection.Forward);
				t.EndPosition = run.ContentStart.GetPositionAtOffset(text.Length, LogicalDirection.Backward);
				t.Word = lastWord;
				tags.Add(t);
			}
		}
		#endregion

		
		#region Public Validation
		public static bool IsHex(string x){
			Regex rgx = new Regex(@"^([a-f0-9]{3}){1,2}$");
			return rgx.IsMatch(x);
		}

		public static bool IsNumber(string x){
			Regex rgx = new Regex(@"[\d]+");
			return rgx.IsMatch(x);
		}

		/// <summary>Returns true if the param contains only letters, numbers or the underscore character.</summary>
		/// <param name="x">string to parse</param>
		/// <returns>true if the param contains only letters, numbers or the underscore character, returns false otheriwse.</returns>
		public static bool IsAlphaNumeric(string x){
			Regex rgx = new Regex(@"^[a-zA-Z0-9_]+$");
			return rgx.IsMatch(x);
		}

		#endregion
	}
}
