﻿using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace SQLGrim {
	/// <summary>
	/// Interaction logic for ConnectionPage.xaml
	/// </summary>
	public partial class ConnectionPage : Page {
		public ConnectionPage() {
			InitializeComponent();
		}

		private void Submit_Click(object sender, RoutedEventArgs e) {

			string testConStr = ConString.Text.Trim();
			MessageLabel.Content = "Testing Connection...";

			using (SqlConnection conn = new SqlConnection()) {
				conn.ConnectionString = testConStr;
				try {
					conn.Open();
					MessageLabel.Content = "Success!";
					UserSettings.ConString = testConStr;
					MainPage mainpage = new MainPage();
					this.NavigationService.Navigate(mainpage);
				}
				catch (SqlException) {
					MessageLabel.Content = "Could not connect.";
				}
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			if(UserSettings.SaveConStringToFile){
				ConString.Text = UserSettings.ConString;
				MessageLabel.Content = "Loaded last used ConnectionString.";
			}
		}
	}
}
