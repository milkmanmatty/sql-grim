﻿using SQLGrim.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SQLGrim {
	public class UserSettings {

		public static readonly string FILE_NAME = "./settings.ini";
		public static readonly int MIN_SETTINGS = 8;

		#region Connection string
		public static string ConString { get; set; }
		public static bool SaveConStringToFile { get; set; }
		#endregion

		#region query
		public static string DefaultQuery { get; set; }
		public static readonly string DEF_DefaultQuery = "SELECT TOP (1000) * FROM ";
		#endregion

		#region scripts
		public static List<Script> Scripts { get; set; }
		public static readonly string SCRIPT_FOLDER_LOC = "./Scripts/";
		public static readonly string SCRIPT_FILE_NAME = "./data.scn";
		#endregion

		#region styling
		public static string ReservedWordsColour { get; set; }
		public static bool   ReservedWordsBolded { get; set; }
		public static string SpecialCharsColour { get; set; }
		public static bool   SpecialCharsBolded { get; set; }
		public static string CommentsColour { get; set; }
		public static int    ScriptsPanelWidth { get; set; }
		public static readonly string DEF_ReservedWordsColour = "#9999ff";
		public static readonly string DEF_SpecialCharsColour = "#c586c0";
		public static readonly string DEF_CommentsColour = "#41A63F";
		public static readonly bool DEF_IsBold = false;
		public static readonly int DEF_ScriptsPanelWidth = 300;
		#endregion

		#region methods
		public static bool LoadSettings(){
			LocateSettings();
			
			Scripts = new List<Script>();
			LoadScripts();

			//ReadAllLines because the file should be small.
			var lines = File.ReadAllLines(FILE_NAME);

			//Some quick validation
			if(lines.Length < MIN_SETTINGS){
				CreateSettings();
				lines = File.ReadAllLines(FILE_NAME);
			}

			string[][] splits = new string[lines.Length][];
			for(int i = 0; i < lines.Length; i++){
				splits[i] = lines[i].Split(new char[] { '=' }, 2, StringSplitOptions.None);
			}

			//Populate data
			ConString	= splits[0][1];
			//Unused 	= splits[1][1];
			SaveConStringToFile	= Boolean.Parse(splits[2][1]);
			DefaultQuery	= splits[3][1];
			ReservedWordsColour	= splits[4][1];
			ReservedWordsBolded	= Boolean.Parse(splits[5][1]);
			SpecialCharsColour	= splits[6][1];
			SpecialCharsBolded	= Boolean.Parse(splits[7][1]);
			CommentsColour	= splits[8][1];
			ScriptsPanelWidth	= Int32.Parse(splits[9][1]);

			return false;
		}

		private static bool LocateSettings(){
			if(File.Exists(FILE_NAME)){
				return true;
			}
			CreateSettings();
			return false;
		}

		private static void CreateSettings(){
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("ConString=");
			sb.AppendLine("Unused=");
			sb.AppendLine("SaveConStringToFile=True");
			sb.AppendLine("DefaultQuery="+DEF_DefaultQuery+"{TABLE}");
			sb.AppendLine("ReservedWordsColour="+DEF_ReservedWordsColour);
			sb.AppendLine("ReservedWordsBolded="+DEF_IsBold);
			sb.AppendLine("SpecialCharsColour="+DEF_SpecialCharsColour);
			sb.AppendLine("SpecialCharsBolded="+DEF_IsBold);
			sb.AppendLine("CommentsColour="+DEF_CommentsColour);
			sb.AppendLine("ScriptsPanelWidth="+DEF_ScriptsPanelWidth);
			File.WriteAllText(FILE_NAME, sb.ToString());
		}

		public static bool SaveToFile(){
			StringBuilder sb = new StringBuilder();
			if(SaveConStringToFile){
				sb.AppendLine("ConString="+ConString);
			} else {
				sb.AppendLine("ConString=");
			}
			sb.AppendLine("Unused=");
			sb.AppendLine("SaveConStringToFile="+SaveConStringToFile);
			sb.AppendLine("DefaultQuery="+DefaultQuery);
			sb.AppendLine("ReservedWordsColour="+ReservedWordsColour);
			sb.AppendLine("ReservedWordsBolded="+ReservedWordsBolded);
			sb.AppendLine("SpecialCharsColour="+SpecialCharsColour);
			sb.AppendLine("SpecialCharsBolded="+SpecialCharsBolded);
			sb.AppendLine("CommentsColour="+CommentsColour);
			sb.AppendLine("ScriptsPanelWidth="+ScriptsPanelWidth);
			try {
				File.WriteAllText(FILE_NAME, sb.ToString());
				return true;
			} catch {
				return false;
			}
		}

		public static void DebugBP(){
			var s = Scripts.ToArray();
			var t = 0;
		}
		#endregion

		#region Save/Load Scripts

		public static bool SaveScripts(){
			if(Scripts.Count <= 0){
				return false;
			}

			//Create folder
			Directory.CreateDirectory(SCRIPT_FOLDER_LOC);

			//Create txt files
			try {
				foreach(var s in Scripts){
					File.WriteAllText(SCRIPT_FOLDER_LOC+s.Name+".txt", s.Query + "\r\n!@##$\r\n" + s.GetTags());
				}

				//Remove existing ZIP
				if(File.Exists(SCRIPT_FILE_NAME)) {
					File.Delete(SCRIPT_FILE_NAME);
				}

				//ZIP all files in folder
				ZipFile.CreateFromDirectory(SCRIPT_FOLDER_LOC,SCRIPT_FILE_NAME);

				//Cleanup
				foreach(var s in Scripts){
					File.Delete(SCRIPT_FOLDER_LOC+s.Name+".txt");
				}
				Directory.Delete(SCRIPT_FOLDER_LOC);
			} catch {
				return false;
			}
			return true;
		}

		private static void LoadScripts() {
			//Check to see if any scripts exist
			if (File.Exists(SCRIPT_FILE_NAME)) {

				//Remove the Scripts folder and everything in it.
				if (Directory.Exists(SCRIPT_FOLDER_LOC)) {
					DirectoryInfo di = new DirectoryInfo(SCRIPT_FOLDER_LOC);
					foreach (FileInfo file in di.GetFiles()) {
						file.Delete();
					}
					foreach (DirectoryInfo dir in di.GetDirectories()) {
						dir.Delete(true);
					}
				}
				//Extract scripts to directory
				ZipFile.ExtractToDirectory(SCRIPT_FILE_NAME, SCRIPT_FOLDER_LOC);

				//Load into memory
				DirectoryInfo di2 = new DirectoryInfo(SCRIPT_FOLDER_LOC);
				foreach (FileInfo file in di2.GetFiles()) {
					var lines = File.ReadAllLines(file.FullName);

					string flat = string.Join("\r\n", lines);
					lines = flat.Split(new string[] { "!@##$" }, StringSplitOptions.None);

					Script toAdd = new Script(){
						Query = lines[0],
						Name = Path.GetFileNameWithoutExtension(file.Name),
						Tags = lines[1].Trim().Split(new char[] { ',' })
					};

					Scripts.Add(toAdd);
				}

				//Remove the Scripts folder and everything in it.
				if (Directory.Exists(SCRIPT_FOLDER_LOC)) {
					DirectoryInfo di = new DirectoryInfo(SCRIPT_FOLDER_LOC);
					foreach (FileInfo file in di.GetFiles()) {
						file.Delete();
					}
					foreach (DirectoryInfo dir in di.GetDirectories()) {
						dir.Delete(true);
					}
				}
			}
		}
		#endregion
	}
}
