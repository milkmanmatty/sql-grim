﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLGrim.Models {
	public class Script {
		public string Name { get; set; }
		public string Query { get; set; }
		public string[] Tags { get; set; }

		public bool ContainsTag(string tag){
			foreach(var t in Tags){
				if(t.IndexOf(tag, StringComparison.OrdinalIgnoreCase) >= 0){
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Flattens the Tag array into a singular string and adds commas inbetween the elements.
		/// </summary>
		/// <returns></returns>
		public string GetTags(){
			string s = "";
			for(int i = 0; i < Tags.Length; i++){
				s+=Tags[i];
				if(i < Tags.Length-1){
					s+=",";
				}
			}
			return s;
		}

		public void AddTag(string tag){
			string[] newArray = new string[Tags.Length+1];
			for(int i = 0; i < Tags.Length; i++){
				newArray[i] = Tags[i];
			}
			newArray[newArray.Length-1] = tag;
			Tags = newArray;
		}
	}
}
