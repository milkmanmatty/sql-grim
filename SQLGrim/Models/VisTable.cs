﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLGrim.Models {
	public class VisTable {

		public string Name { get; set; }
		public string Extra { get; set; }

		public override string ToString() {
			return Name;
		}

		public VisTable(DataRow dr){
			if(TextFormat.reservedWords.Contains(dr.ItemArray[2].ToString().ToUpper())){
				Name = dr.ItemArray[0] + "." + dr.ItemArray[1] + ".[" + dr.ItemArray[2] + "]";
			} else {
				Name = dr.ItemArray[0] + "." + dr.ItemArray[1] + "." + dr.ItemArray[2];
			}
			Extra = ""+dr.ItemArray[3];
		}
	}
}
